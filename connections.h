#pragma once

#include <armadillo>
#include <cmath>
#include "IsingModel.h"

using namespace std;
using namespace arma;

IsingModel::connection trim_isolated(IsingModel::connection);
IsingModel::connection square_free_with_hole_connection(uword, uword, uword);
IsingModel::connection square_periodic_connection(uword, uword);
IsingModel::connection square_free_connection(uword, uword);
IsingModel::connection honeycomb(uword);

IsingModel::connection trim_isolated(IsingModel::connection A) {
  for (uword i = A.n_rows; i--;) {
    if (A.row(i).n_nonzero > 0 || A.col(i).n_nonzero > 0) continue;
    A.shed_row(i);
    A.shed_col(i);
  }
  return A;
}

IsingModel::connection square_free_with_hole_connection(uword nrow, uword ncol,
                                                        uword m) {
  auto A = square_free_connection(nrow, ncol);
  auto s = size(nrow, ncol);
  uword a = (nrow + 1) / 2 - m / 2;
  uword b = a + m - 1;
  uword c = (ncol + 1) / 2 - m / 2;
  uword d = c + m - 1;

  for (uword i = b; i >= a; --i)
    for (uword j = d; j >= c; --j) {
      A.row(sub2ind(s, i, j)) *= 0;
      A.col(sub2ind(s, i, j)) *= 0;
    }
  return trim_isolated(A);
}

IsingModel::connection square_sheet_connection(uword nrow, uword ncol,
                                               uword nslice) {
  IsingModel::connection A(nrow * ncol * nslice, nrow * ncol * nslice);

  auto s = size(nrow, ncol, nslice);
  const uword p = nrow - 1;
  const uword q = ncol - 1;
  const uword r = nslice - 1;

  for (uword i = nrow; i--;)
    for (uword j = ncol; j--;)
      for (uword k = nslice; k--;) {
        auto R = A.row(sub2ind(s, i, j, k));
        R(sub2ind(s, (i + 1) % nrow, j, k)) = 1;
        R(sub2ind(s, (i + p) % nrow, j, k)) = 1;
        R(sub2ind(s, i, (j + 1) % ncol, k)) = 1;
        R(sub2ind(s, i, (j + q) % ncol, k)) = 1;
        if (k < r) R(sub2ind(s, i, j, k + 1)) = 1;
        if (k > 0) R(sub2ind(s, i, j, k - 1)) = 1;
      }
}

IsingModel::connection square_periodic_connection(uword nrow, uword ncol) {
  IsingModel::connection A(nrow * ncol, nrow * ncol);

  auto s = size(nrow, ncol);
  const uword p = nrow - 1;
  const uword q = ncol - 1;

  for (uword i = nrow; i--;)
    for (uword j = ncol; j--;) {
      auto R = A.row(sub2ind(s, i, j));
      R(sub2ind(s, (i + 1) % nrow, j)) = 1;
      R(sub2ind(s, (i + p) % nrow, j)) = 1;
      R(sub2ind(s, i, (j + 1) % ncol)) = 1;
      R(sub2ind(s, i, (j + q) % ncol)) = 1;
    }
  return A;
}

IsingModel::connection square_free_connection(uword nrow, uword ncol) {
  IsingModel::connection A(nrow * ncol, nrow * ncol);

  auto s = size(nrow, ncol);
  const uword p = nrow - 1;
  const uword q = ncol - 1;

  for (uword i = nrow; i--;)
    for (uword j = ncol; j--;) {
      auto R = A.row(sub2ind(s, i, j));
      if (i < p) R(sub2ind(s, i + 1, j)) = 1;
      if (i > 0) R(sub2ind(s, i - 1, j)) = 1;
      if (j < q) R(sub2ind(s, i, j + 1)) = 1;
      if (j > 0) R(sub2ind(s, i, j - 1)) = 1;
    }
  return A;
}

IsingModel::connection hexagon_periodic_connection(uword layer) {
  auto sub2ind = [](uword i, uword j, sword k) -> uword {
    sword d = 2 * i + 1;
    while (k < 0) {
      j += 5;
      k += d;
    }
    if (k >= d) {
      j += k / d;
      k %= d;
    }
    j %= 6;
    return 6 * i * i + d * j + k;
  };

  IsingModel::connection A(6 * layer * layer, 6 * layer * layer);
  for (uword i = layer; i--;) {
    for (uword j = 6; j--;)
      for (uword k = 2 * i + 1; k--;) {
        auto R = A.row(sub2ind(i, j, k));

        R(sub2ind(i, j, k - 1)) = 1;
        R(sub2ind(i, j, k + 1)) = 1;

        if (k % 2)
          R(sub2ind(i - 1, j, k - 1)) = 1;
        else if (i == layer - 1)
          R(sub2ind(i, j + 4, -k - 1)) = 1;
        else
          R(sub2ind(i + 1, j, k + 1)) = 1;
      }
  }
  return A;
}

// rings = number of rings = 3 for hexagonal lattice with 19 hexagons
// rings = 2 for hexagonal lattice with 7 hexagons
IsingModel::connection honeycomb(uword rings) {
  uword topLayerSpins = 3 + 2 * (rings - 1);
  // total number of occupied sites
  uword NumberOfSpins = 6 * rings * rings;

  // periodic boundary condition switch
  bool PBC = true;
  uvec top(topLayerSpins - 1);
  uvec bottom(topLayerSpins - 1);
  uvec topLeft(topLayerSpins - 1);
  uvec topRight(topLayerSpins - 1);
  uvec bottomLeft(topLayerSpins - 1);
  uvec bottomRight(topLayerSpins - 1);
  uword TL, TR, BL, BR;
  TL = TR = BL = BR = 0;
  uword temp;

  // top boundary
  for (uword i = 0; i < topLayerSpins - 1; i++) {
    top(i) = i;
  }

  // bottom boundary
  for (uword i = 0; i < topLayerSpins - 1; i++) {
    bottom(i) = NumberOfSpins - (topLayerSpins - 1 - i);
  }

  // top-left boundary
  topLeft(0) = 0;
  TL++;

  // construct NumberOfSpins x NumerOfSpins adjancency matrix
  IsingModel::connection A(NumberOfSpins, NumberOfSpins);

  uword SpinInRow = 0;
  bool stick = true;
  for (uword i = 0; i < rings; i++) {
    stick = true;
    for (uword j = SpinInRow; j < (SpinInRow + topLayerSpins + 2 * i); j++) {
      if (j < (SpinInRow + topLayerSpins + 2 * i - 1)) {
        A(j, j + 1) = 1;
        A(j + 1, j) = 1;
      }
      if (stick) {
        if (i < rings - 1) {
          A(j, j + topLayerSpins + 2 * i + 1) = 1;
          A(j + topLayerSpins + 2 * i + 1, j) = 1;
        } else {
          A(j, j + topLayerSpins + 2 * i) = 1;
          A(j + topLayerSpins + 2 * i, j) = 1;
        }
        stick = false;
      } else
        stick = true;

      // boundaries
      if (i != 0) {
        if (j <= SpinInRow + 1) {
          topLeft(TL) = j;
          TL++;
        }
      }
      if (j >= SpinInRow + topLayerSpins + 2 * i - 2) {
        topRight(TR) = j;
        TR++;
      }
    }

    SpinInRow += topLayerSpins + 2 * i;
  }

  // boundaries
  bottomRight(0) = SpinInRow - 1;
  BR++;
  topLeft(TL) = SpinInRow;
  TL++;

  for (uword i = rings - 1; i >= 0; i--) {
    stick = false;
    for (uword j = SpinInRow; j < (SpinInRow + topLayerSpins + 2 * i); j++) {
      if (j < (SpinInRow + topLayerSpins + 2 * i - 1)) {
        A(j, j + 1) = 1;
        A(j + 1, j) = 1;
      }
      if (i != 0) {
        if (stick) {
          A(j, j + topLayerSpins + 2 * i - 1) = 1;
          A(j + topLayerSpins + 2 * i - 1, j) = 1;
          stick = false;
        } else
          stick = true;
      }

      // boundaries
      if (j <= SpinInRow + 1) {
        bottomLeft(BL) = j;
        BL++;
      }

      if (i != 0) {
        if (j >= SpinInRow + topLayerSpins + 2 * i - 2) {
          bottomRight(BR) = j;
          BR++;
        }
      }
    }
    SpinInRow += topLayerSpins + 2 * i;

    if (i == 0) break;
  }

  // boundaries
  bottomRight(BR) = NumberOfSpins - 1;
  BR++;

  if (rings > 1) {
    for (uword i = 1; i <= topLayerSpins - 4; i += 2) {
      temp = topLeft(i);
      topLeft(i) = topLeft(i + 1);
      topLeft(i + 1) = temp;

      temp = bottomRight(i);
      bottomRight(i) = bottomRight(i + 1);
      bottomRight(i + 1) = temp;
    }
  }

  if (PBC) {
    for (uword i = 0; i < topLayerSpins - 1; i++) {
      A(top(i), bottom(i)) = 1;
      A(bottom(i), top(i)) = 1;
      A(bottomRight(i), topLeft(i)) = 1;
      A(topLeft(i), bottomRight(i)) = 1;
      A(bottomLeft(i), topRight(i)) = 1;
      A(topRight(i), bottomLeft(i)) = 1;
    }
  }

  return A;
}
