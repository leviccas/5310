#pragma once

#include <random>
#include <armadillo>

using namespace std;
using namespace arma;

class IsingModel {
 public:
  typedef Col<sword> state;
  typedef SpMat<sword> connection;
  typedef uniform_int_distribution<sword> intdist;
  typedef mt19937_64 RandomEngine;
  typedef uword change;

  const uword n;
  const double B;
  const connection A;
  RandomEngine& engine;

  IsingModel(connection&& A, const double B, RandomEngine& engine)
      : A(A), B(B), n(A.n_cols), engine(engine) {}

  IsingModel(const connection& A, const double B, RandomEngine& engine)
      : A(A), B(B), n(A.n_cols), engine(engine) {}

  double energy(const state& S) const;
  double magnetization(const state& S) const;
  double energy_change(const state& S, change C) const;

  state rand();
  change next(const state& S);
  void apply_change(state& S, change C) const;
};

double IsingModel::energy(const IsingModel::state& S) const {
  return as_scalar(-S.t() * A * S / 2) - B * accu(S);
}

double IsingModel::magnetization(const IsingModel::state& S) const {
  return sum(S);
}

double IsingModel::energy_change(const IsingModel::state& S,
                                 IsingModel::change C) const {
  return (B + as_scalar(A.row(C) * S)) * 2 * S(C);
}

IsingModel::state IsingModel::rand() {
  IsingModel::intdist distr(0, 1);

  IsingModel::state S(n);
  S.imbue([&]() { return distr(engine) * 2 - 1; });
  return S;
}

// Don't change the interface/argument
// simply because the method do not use the state!!
IsingModel::change IsingModel::next(const IsingModel::state& S) {
  IsingModel::intdist distr(0, n - 1);

  return distr(engine);
}

void IsingModel::apply_change(IsingModel::state& S,
                              IsingModel::change C) const {
  S(C) *= -1;
}
