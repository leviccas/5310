#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <tuple>

#include <armadillo>

#include "IsingModel.h"
#include "connections.h"
#include "MonteCarlo.h"
#include "vector"

using namespace std;
using namespace arma;

IsingModel::RandomEngine engine(time(nullptr));

tuple<double, double, double, double> calculate_mean_energy_magnetization(
    const IsingModel::connection& A, const double B, const double T) {
  IsingModel model = IsingModel(A, B, engine);
  // Forgot the value of k; set it as 1 for now.
  double k = 1;
  double beta = 1 / (k * T);

  running_stat<double> energy;
  running_stat<double> magnetization;

  MonteCarlo<IsingModel> mc(model, beta, engine);
  for (uword i = 2000 * mc.model.n; i-- > 0;) {
    mc.iterate();
  }
  for (uword i = 18000 * mc.model.n; i-- > 0;) {
    mc.iterate();
    energy(model.energy(mc.current_state));
    magnetization(model.magnetization(mc.current_state));
  }
  return make_tuple(energy.mean(), energy.var(), magnetization.mean(), magnetization.var());
}

void test_print(ostream& os, const IsingModel::connection& A, const double B,
                const double T) {
  IsingModel model = IsingModel(A, B, engine);

  // Forgot the value of k; set it as 1 for now.
  double k = 1;
  double beta = 1 / (k * T);

  MonteCarlo<IsingModel> mc(model, beta, engine);

  for (uword i = 20000 * mc.model.n; i-- > 0;) {
    for (auto&& s : mc.current_state) os << (s > 0 ? '+' : '-');
    os << endl;
    mc.iterate();
  }
}

namespace part_a {
const int J = 1;
const double B = 0;
const vector<double> Ts = {0.1, 0.25, 0.5, 0.75, 1., 1.25, 1.5, 1.75, 2., 5.};
const IsingModel::connection A = J * square_periodic_connection(5, 5);

void test(ostream& os) {
  for (auto&& T : Ts) {
    auto result = calculate_mean_energy_magnetization(A, B, T);
    os << T << ',' << get<0>(result) << ',' << get<1>(result) << ','
       << get<2>(result) << get<3>(result) <<endl;
  };
}
}  // part_a

namespace part_b {
const int& J = part_a::J;
const double& B = part_a::B;
const IsingModel::connection& A = part_a::A;

void test(ostream& os) {
  for (double T = 0; T <= 5; T += 0.1) {
    os << T;
    for (uword trial = 5; trial--;) {
      auto result = calculate_mean_energy_magnetization(A, B, T);
      os << ',' << get<2>(result);
    }
    os << endl;
  };
}
}  // part_b

namespace part_c {
const int& J = part_a::J;
const double& B = part_a::B;
const IsingModel::connection& A = part_a::A;

void test(ostream& os) {
  for (double T = 0; T <= 5; T += 0.1) {
    os << T;
    for (uword trial = 5; trial--;) {
      auto result = calculate_mean_energy_magnetization(A, B, T);
      os << ',' << get<1>(result)/(T*T);
    }
    os << endl;
  };
}
}  // part_c

namespace part_f {
const int& J = part_a::J;
const double& B = part_a::B;
const vector<double>& Ts = part_a::Ts;
const IsingModel::connection A = J * hexagon_periodic_connection(3);

void test(ostream& os) {
  for (auto&& T : Ts) {
    auto result = calculate_mean_energy_magnetization(A, B, T);
    os << T << ',' << get<0>(result) << ',' << get<1>(result) << ','
       << get<2>(result) <<  ',' << get<3>(result) << endl;
  };
}
}

int main(int argc, char const* argv[]) {
  // part_f::test(cout);

  return 0;
}