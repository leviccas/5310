#pragma once

#include <cmath>
#include <random>

template <typename Model>
class MonteCarlo {
 public:
  typedef mt19937_64 RandomEngine;

  double beta;
  Model model;
  typename Model::state current_state;
  RandomEngine& engine;

  MonteCarlo(Model&& model, double beta, RandomEngine& engine)
      : model(model), beta(beta), current_state(model.rand()), engine(engine) {}

  MonteCarlo(Model& model, double beta, RandomEngine& engine)
      : model(model), beta(beta), current_state(model.rand()), engine(engine) {}

  void iterate();
};

template <typename Model>
void MonteCarlo<Model>::iterate() {
  auto change = model.next(current_state);
  auto dE = model.energy_change(current_state, change);

  if (dE > 0) {
    uniform_real_distribution<double> distr(0, 1);
    double p = exp(-beta * dE);
    double q = distr(engine);
    if (p < q) return;
  }
  model.apply_change(current_state, change);
}
