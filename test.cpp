#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <tuple>
#include <cstdlib>
#include <armadillo>

#include "IsingModel.h"
#include "connections.h"
#include "MonteCarlo.h"
#include "vector"

int main(int argc, char const* argv[]) {
  ofstream fout;
  uword n = atoi(argv[1]);

  fout.open(argv[2]);
  fout << hexagon_periodic_connection(n) << endl;
  fout.close();

  fout.open(argv[3]);
  fout << honeycomb(n) << endl;
  fout.close();

  return 0;
}